## Videos
Some good videos regarding code code formatting 

### Formatting 

- [NoComment](https://www.youtube.com/watch?v=Bf7vDBBOBUA)
- [NeverNesting](https://www.youtube.com/watch?v=CFRhGnuXG-4)
- [PrematureOptimization](https://www.youtube.com/watch?v=tKbV6BpH-C8)

### Subjects 

- [Art of Programming](https://www.youtube.com/watch?v=6avJHaC3C2U)
